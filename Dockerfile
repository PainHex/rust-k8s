FROM ekidd/rust-musl-builder as builder

WORKDIR /home/rust/

COPY Cargo.toml .
COPY src/main.rs ./src/

RUN rustup target add x86_64-unknown-linux-musl
RUN cargo build --release
RUN sudo touch src/main.rs

FROM scratch
WORKDIR /home/rust
COPY --from=builder /home/rust/target/x86_64-unknown-linux-musl/release/http_server /app/http_server
EXPOSE 8080
CMD ["/app/http_server"]
